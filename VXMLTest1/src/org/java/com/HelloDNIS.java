package org.java.com;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HelloDNIS
 */
@WebServlet("/HelloDNIS")
public class HelloDNIS extends HttpServlet {
	
		 public void doGet( HttpServletRequest req, HttpServletResponse res)
		  throws IOException, ServletException
		 {
		  res.setContentType("text/xml");
		  PrintWriter out = res.getWriter();
		  out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?> " );
		  out.println("<vxml version=\"2.0\" xmlns=\"http://www.w3.org/2001/vxml\"\r\n" + 
		  		"  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \r\n" + 
		  		"  xsi:schemaLocation=\"http://www.w3.org/2001/vxml \r\n" + 
		  		"   http://www.w3.org/TR/voicexml20/vxml.xsd\">");

		  out.println("  <form>");
		  out.println("   <field name=\"collectedDnis\" type=\"digits?minlength=4;maxlength=5\"> ");
		  out.println("     <prompt> Enter your 5 digit DNIS </prompt>");
		  out.println("      <filled>");
		  out.println("   <assign name=\"dnis\" expr=\"collectedDnis\"/>");
		  out.println("  </filled>");
		  out.println("<reprompt/>");
		  out.println("</catch>");
		  out.println("</field>");
		  out.println("<subdialog name=\"result\" src=\"\" namelist=\"dnis\">");
		  out.println("<filled>");
		  out.println("<prompt> the returned values are...</prompt>");
		  out.println("<log>the returned values are...</log>");
		  out.println("</filled>");
		  out.println("</subdialog>");
		  out.println("</form>");
		  out.println("</vxml>");
		 } 
		}
